
### What is this repository for? ###
This repo is for using ransac to segment an object from a pointcloud.

### How do I get set up? ###

This needs ROS indigo, PCL and Opencv.

### Git Tags for Our Grasp ISRR 2017 Paper###
Object segmentation for real robot with Kinect2 camera: tag Grasp_segmenation_real_robot_kinect2 on master branch. 
Object segmentation for Gazebo dart simulation with Blensor camera: tag Seg_sim_isrr_2017 on master branch. 
